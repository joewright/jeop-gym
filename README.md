# The J Gym
This is a gym to train your skills for the online Jeopardy! test.

The user interface during the training mimics the Jeopardy online test.

Our database contains question and category data from all historical episodes of Jeopardy. We scraped the J Archive without their permissions to do this.

When you start a test, you get 40 random questions sampled from the dataset. These questions are presented in the same manner as the Jeopardy online test.

When you finish, you'll see a report that lists the questions you answered, how you answered, and what the J Archive answer was for each.