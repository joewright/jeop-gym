/**
 * J! Scraper
 * @author Joe Wright
 * Module for scraping J! game objects from J! Archive HTML pages
 */
const path = require('path');
const fs = require('fs');

const request = require('request');
const htmlparser2 = require('htmlparser2');

exports.getGame = getGame;
/**
 * JScrape.getGame
 * @param  {number} gameId game ID to retrieve from the archive
 * @return {JGameObject} a J game object with jArchiveId, categories, clues, title, and comments
 */
async function getGame(gameId) {
	const gameURL = `http://www.j-archive.com/showgame.php?game_id=${gameId}`;
	const gameText = await doReq(gameURL);
	fs.writeFileSync('haha.html', gameText);

	const game = {
		jArchiveId: gameId,
		categories: [],
		clues: [],
		title: '',
		comments: ''
	};
	const categoryName = 'category_name';
	const categoryComments = 'category_comments';
	const clue = 'clue';
	const clueValue = 'clue_value';
	const clueOrder = 'clue_order_number';
	const clueResponse = 'correct_response';
	const clueText = 'clue_text';

	let currentCategory = {};
	let currentGameData = {};
	let currentClue = {};
	let getRound = false;
	let currentRound = '';

	const parser = new htmlparser2.Parser({
		onopentag(name, attribs) {
			// event called when a DOM node is opened
			if (name === 'h2') {
				getRound = true;
			}
			if (name === 'div') {
				// found a game title
				if (attribs.id === 'game_title') {
					currentGameData.titleOpen = true;
				}
				// found game comments
				if (attribs.id === 'game_comments') {
					currentGameData.comments = true;
				}
			}
			if (name === 'td' && attribs.class) {
				// found a category
				if (attribs.class === categoryName) {
					currentCategory.name = true;
				}
				if (attribs.class === categoryComments) {
					currentCategory.comments = true;
				}
				if (attribs.class === clue) {
					if (currentClue.value) {
						game.clues.push(currentClue);
					}
					currentClue = {
						round: currentRound.replace(' Round', '')
					};
				}
				if (attribs.class === clueValue) {
					currentClue.value = true;
				}
				if (attribs.class === clueOrder) {
					currentClue.order = true;
				}
				if (attribs.class === clueText && !currentClue.text) {
					currentClue.text = true;
					currentClue.id = attribs.id;
					currentClue.category = '';
					let ixm = 0;
					if (attribs.id.startsWith('clue_DJ')) {

						ixm = 5;
					}
					let cIndex = parseInt(
						attribs.id.split('J_').pop().split('_').shift()) + ixm - 1;
					if (attribs.id === 'clue_FJ') {
						cIndex = 12;
					}
					const cMatch = game.categories[cIndex];
					if (cMatch) {
						currentClue.category = cMatch.name;
					}
				}
			}
			if (name === 'em' && attribs && attribs.class === clueResponse) {
				currentClue.response = true;
			}
			// track title text
			if (name === 'h1' && currentGameData.titleOpen) {
				currentGameData.title = true;
				delete currentGameData.titleOpen;
			}
		},
		ontext(text) {
			// called immediately after DOM node opened and text parsed
			if (getRound === true) {
				getRound = false;
				currentRound = text;
			}
			if (currentCategory.name === true) {
				currentCategory.name = text;
			}
			if (currentCategory.comments === true) {
				currentCategory.comments = text;
				if (['\n', '\n  '].includes(text)) {
					delete currentCategory.comments;
				}
				game.categories.push(Object.assign({}, currentCategory));
				currentCategory = {};
			}
			if (currentGameData.title === true) {
				currentGameData.title = text;
				game.title = text;
			}
			if (currentGameData.comments === true) {
				currentGameData.comments = text;
				game.comments = text.replace(/\\n/g, '').trim();
			}

			// clues
			if (currentClue.value === true) {
				currentClue.value = text;
			}
			if (currentClue.order === true) {
				currentClue.order = text;
			}
			if (currentClue.response === true) {
				currentClue.response = text;
			}
			if (currentClue.text === true) {
				currentClue.text = text;
			}
		},
		onend() {
			// after we finish parsing, write everything to a file
			if (currentClue && currentClue.id) {
				currentClue.value = '$0';
				currentClue.order = 0;
				game.clues.push(currentClue);
			}
			game.clues = game.clues.map(cl => {
				cl.value = parseInt(cl.value.replace('$', ''));
				cl.order = parseInt(cl.order);
				return cl;
			});
			fs.writeFileSync(path.join(__dirname, `${gameId}.json`), JSON.stringify(game));
		}
	}, {
		decodeEntities: false
	});
	parser.write(gameText);
	parser.end();
}

function doReq(url, options) {
	return new Promise((resolve, reject) => {
		request(url, options, (error, res, body) => {
			if (error) {
				return reject(error);
			}
			resolve(body);
		});
	});
}