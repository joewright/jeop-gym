const path = require('path');
const fs = require('fs');

module.exports.getRandomGame = function getRandomGame() {
	const allGames = fs.readdirSync(
		path.join(__dirname, '../../bin'))
		.filter(fn => fn.endsWith('.json'));

	const fname = allGames[Math.floor(Math.random() * allGames.length)];
	const game = require(path.join(__dirname, `../../bin/${fname}`));
	return game;
};

module.exports.getRandomClues = function getRandomClues(game, nClues) {
	let clues = game.clues;
	clues.reverse();
	let final = clues[0];
	// exclude final in the first set, add it to the end of the list
	let selection = clues.sort(function (ia, ib) {
		var val = 0.5 - Math.random();
		if (ia.round.startsWith('Final') || ib.round.startsWith('Final')) {
			return -1;
		}
		return val;
	});

	return selection.slice(0, nClues - 1).concat([final]);
};