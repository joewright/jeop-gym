const path = require('path');
const express = require('express');
const morgan = require('morgan');
const JeopData = require('./lib/jeop-data');
const PORT = process.env.PORT || 3000;
const app = exports.app = express();

// static website content
const VendorBootstrap = path.join(__dirname, '../node_modules/bootstrap/dist');
const clientContent = path.join(__dirname, '../client');
app.use('/vendor/bootstrap', express.static(VendorBootstrap));
app.use(express.static(clientContent));
app.use(morgan('short'));

if (process.argv[1].endsWith('server/') || process.argv[1].endsWith('index.js')) {
	app.listen(PORT, () => {
		console.log(`Listening at port ${PORT}`);
	});
}

app.get('/exam', (req, res) => {
	// send back some clues
	let nQuestions = 12;
	nQuestions = parseInt(req.query.questions);
	// validate query param
	if (nQuestions % 2 === 1) {
		nQuestions--;
	}
	if (isNaN(nQuestions) ||
		nQuestions < 0 ||
		nQuestions % 2 === 1 ||
		nQuestions > 55) {
		nQuestions = 12;
	}
	const game = JeopData.getRandomGame();
	res.send({
		game: game,
		clues: JeopData.getRandomClues(game, nQuestions)
	});
});