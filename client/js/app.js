const TagsRE = /<\/?[^>]+(>|$)/g;
const DOMUtils = {
	updateClass: (elem, className, on) => {
		elem.className = elem.className || '';
		if (elem.className.includes(className)) {
			elem.className = elem.className.replace(className, '');
		}
		if (on) {
			elem.className = `${elem.className} ${className}`;
		}
	},
	addClass: function(elem, className) {
		this.updateClass(elem, className, true);
	},
	removeClass: function(elem, className) {
		this.updateClass(elem, className);
	},
	decodeHtml: function(html) {
		var txt = document.createElement("textarea");
		txt.innerHTML = html;
		return txt.value.replace(TagsRE, '').replace(/\\'/g, '\'');
	}
};

const App = {
	// HTML element references
	gameLink: document.getElementById('game-link'),
	clueForm: document.getElementById('current-clue-form'),
	currentCategory: document.getElementById('current-category'),
	currentClue: document.getElementById('current-clue'),
	answerInput: document.getElementById('current-answer'),
	loading: document.getElementById('loading'),
	results: document.getElementById('results'),
	resultsContent: document.getElementById('results-content'),
	resultTemplate: '',
	nextGameButton: document.getElementById('next-game-button'),
	// game state
	state: {
		clueIndex: 0,
		answer: '',
		clues: []
	},
	// set DOM event listeners, load the first game
	init: async function() {
		this.resultTemplate = this.resultsContent.innerHTML;
		// listen to DOM events
		this.answerInput.addEventListener('keyup', this.answerUpdated());
		this.clueForm.addEventListener('submit', this.onSubmit());
		this.nextGameButton.addEventListener('click', this.nextGameClicked());

		await this.nextGame();
	},
	// game loaded, draw game DOM and show the first clue
	ready(data) {
		// prepare DOM
		DOMUtils.addClass(this.loading, 'd-none');
		DOMUtils.addClass(this.results, 'd-none');
		DOMUtils.removeClass(this.clueForm, 'd-none');

		// start on the 1st clue
		this.state.clueIndex = -1;
		this.nextClue();
	},
	// user updated the answer input
	answerUpdated() {
		return (event) => {
			this.state.answer = event.target.value;
		};
	},
	// user submitted their answer
	onSubmit() {
		return (event) => {
			event.preventDefault();
			if (this.state.clues[this.state.clueIndex]) {
				this.state.clues[this.state.clueIndex].submittedAnswer = this.state.answer;
				this.nextClue();
			}
		};
	},
	// @todo
	// answer timer

	// handle next game button click
	nextGameClicked() {
		return () => {
			this.nextGame();
		};
	},
	// get the next clue; show results after the final clue
	nextClue() {
		this.state.answer = '';
		this.answerInput.value = '';
		this.state.clueIndex++;
		const clue = this.state.clues[this.state.clueIndex];
		// show results!
		if (!clue) {
			return this.showResults();
		}

		this.currentCategory.innerText = clue.category;
		this.currentClue.innerText = clue.text;
		this.answerInput.focus();
	},
	// show the answers to all of the clues and display the next game button
	showResults() {
		// prepare DOM
		DOMUtils.addClass(this.clueForm, 'd-none');
		DOMUtils.removeClass(this.results, 'd-none');

		this.resultsContent.innerHTML = '';
		// render template for each answer
		var byCategory = {};
		this.state.clues.forEach(clue => {
			if (!byCategory[clue.category]) {
				byCategory[clue.category] = [clue];
			} else {
				byCategory[clue.category].push(clue);
			}
		});
		Object.keys(byCategory).forEach(category => {
			let rt = this.resultTemplate;
			let rows = byCategory[category].sort((ia, ib) => {
				return ia.value > ib.value;
			});
			rows.forEach((clue, idx) => {
				let result = rt;
				Object.keys(clue).forEach(qKey => {
					if (idx > 0 && qKey === 'category') {
						result = result.replace(`{{ clue.${qKey} }}`, '');
						return;
					}
					result = result.replace(`{{ clue.${qKey} }}`, clue[qKey])
				});
				this.resultsContent.innerHTML += result;
			});
		});
	},
	// load another game
	async nextGame() {
		// load game data
		const resp = await fetch('/exam?questions=6');
		const data = await resp.json();

		// set clues data, replace HTML entities
		this.state.clues = data.clues.map(qq => {
			qq.text = DOMUtils.decodeHtml(qq.text);
			qq.response = DOMUtils.decodeHtml(qq.response);
			qq.category = DOMUtils.decodeHtml(qq.category);
			return qq;
		});

		let gameRef = this.gameLink.getAttribute('href')
			.replace(/[0-9]/g, '')
			.replace('{{ gameId }}', '');
		gameRef += data.game.jArchiveId;
		this.gameLink.setAttribute('href', gameRef);
		// prepare DOM
		this.ready(data);
	}
};

App.init();